from app import app
from flask_cors import CORS

cors = CORS(app)

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8080)